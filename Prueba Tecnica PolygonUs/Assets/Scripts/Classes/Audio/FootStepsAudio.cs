using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class FootStepsAudio : MonoBehaviour
{
    //Assignables
    [Header("References")]
    [SerializeField] private AudioSource audioSource;

    [Header("Sound Library")]
    [SerializeField] private List<AudioClip> clips = new List<AudioClip>();

    public void PlayFootStep()
    {
        if (!audioSource || clips.Count == 0) return;

        int libraryIndex = Random.Range(0, clips.Count);

        audioSource.PlayOneShot(clips.ElementAt(libraryIndex));
    }
}
