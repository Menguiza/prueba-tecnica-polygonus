using UnityEngine;

[RequireComponent(typeof(AudioSource))]
public class CubeAudio : MonoBehaviour
{
    //Assignable
    [Header("Sound")]
    [SerializeField] private AudioClip move;
    [SerializeField] private AudioClip barrier;

    //Utility
    private AudioSource source;

    private void Awake()
    {
        //Get reference
        source = GetComponent<AudioSource>();
    }

    private void Start()
    {
        CubeEvents.OnCubeMove += Moved;
        CubeEvents.OnCubeBlocked += Collided;
    }

    private void OnDestroy()
    {
        CubeEvents.OnCubeMove -= Moved;
        CubeEvents.OnCubeBlocked -= Collided;
    }

    #region Behaviors

    private void Moved(GameObject cube)
    {
        if (move) source.PlayOneShot(move);
    }

    private void Collided(Vector3 x, Vector3 y)
    {
        if (barrier) source.PlayOneShot(barrier);
    }

    #endregion
}
