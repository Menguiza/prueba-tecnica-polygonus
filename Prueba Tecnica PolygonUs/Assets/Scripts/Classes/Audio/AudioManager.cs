using UnityEngine;

public class AudioManager : MonoBehaviour
{
    //Assignable
    [Header("Audio Sources")]
    [SerializeField] private AudioSource sfxUI;

    private void Awake()
    {
        UIEvents.OnUISFX += PlaySFXNotSpatial;
    }

    private void OnDestroy()
    {
        UIEvents.OnUISFX -= PlaySFXNotSpatial;
    }

    #region Utility

    private void PlaySFXNotSpatial(AudioClip clip)
    {
        sfxUI.PlayOneShot(clip);
    }

    #endregion
}
