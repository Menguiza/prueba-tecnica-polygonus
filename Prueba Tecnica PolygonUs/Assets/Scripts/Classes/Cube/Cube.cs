using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

[RequireComponent(typeof(LineRenderer))]
public class Cube : MonoBehaviour, IPointerDownHandler, IInstructor
{
    //Assignable
    [Header("Cube Pathing")]
    [SerializeField] private List<EDirections> instructions = new List<EDirections>();

    [Header("Cube Line")]
    [SerializeField] private Transform points;

    [Header("Cube Outline")]
    [SerializeField] private MeshRenderer cubeRenderer;

    //Utility
    private LineRenderer lineRenderer;
    private Vector3[] transformsToPoints;
    private Vector3 initialPos;

    //Accesor
    public List<EDirections> Instructions { get => instructions; }
    public MeshRenderer CubeRenderer { get => cubeRenderer; }
    public Vector3 InitialPos { get => initialPos; }

    private void Awake()
    {
        lineRenderer = GetComponent<LineRenderer>();
        lineRenderer.positionCount = points.childCount;

        transformsToPoints = new Vector3[lineRenderer.positionCount];

        initialPos = transform.localPosition;

        CubeEvents.OnToggleLines += ToggleLine;
    }

    private void Start()
    {
        DrawPathLine();
    }

    private void OnDestroy()
    {
        CubeEvents.OnToggleLines -= ToggleLine;
    }

    #region Behaviors

    public void DrawPathLine()
    {
        lineRenderer.SetPositions(FillVectorArray());
    }

    #endregion

    #region Utility

    private Vector3[] FillVectorArray()
    {
        for (int i = 0; i < lineRenderer.positionCount; i++)
        {
            transformsToPoints[i] = points.GetChild(i).localPosition;
        }

        return transformsToPoints;
    }

    private void ToggleLine()
    {
        lineRenderer.enabled = !lineRenderer.enabled;
    }

    #endregion

    #region Implementation

    public void OnPointerDown(PointerEventData eventData)
    {
        if (eventData.button == 0)
        {
            CubeEvents.InvokeCubeSelect(transform);
        }
    }

    #endregion
}
