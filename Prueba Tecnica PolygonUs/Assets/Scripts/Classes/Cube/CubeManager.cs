using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Unity.VisualScripting;
using UnityEngine;

public class CubeManager : MonoBehaviour
{
    //Assignable
    [Header("References")]
    [SerializeField] private Transform map;

    [Header("Selection Related")]
    [SerializeField] private GameObject arrows;
    [SerializeField][Range(1f, 1.06f)] private float outlineThickness = 1.04f;

    [Header("Grid Parameters")]
    [SerializeField]
    [Tooltip("Conversion ratio: 3x3x3 matrix = 1 unit radius (positive limit, negative limit)")]
    private Vector3Int gridDimensions = new Vector3Int(3, 3, 3);
    [SerializeField]
    [Tooltip("Conversion ratio: 1 unit radius (positive limit, negative limit) = 3x3x3 matrix")]
    private Vector2 gridLimits = new Vector2(1, -1);

    //IoC
    private IMoveService moveService;

    //Utility
    private Cube cube;
    private GameObject[,,] cubeSlots;

    private void Awake()
    {
        //Create Matrix
        cubeSlots = new GameObject[gridDimensions.x, gridDimensions.y, gridDimensions.z];

        CubeEvents.OnArrowPressed += HandleCubeMove;
        CubeEvents.OnCubeSelected += HandleCubeSelection;
        UIEvents.OnSolving += Solve;
        GameEvents.OnRandomizeCubes += RandomizeCubes;
    }

    void Start()
    {
        //SetUp
        FillMatrix();

        moveService = new MoveCubeService();
    }

    private void OnDestroy()
    {
        CubeEvents.OnArrowPressed -= HandleCubeMove;
        CubeEvents.OnCubeSelected -= HandleCubeSelection;
        UIEvents.OnSolving -= Solve;
        GameEvents.OnRandomizeCubes -= RandomizeCubes;
    }

    #region Handlers

    private void HandleCubeMove(IMovementBehavior behavior)
    {
        //Null Reference Check
        if (!cube) return;

        //Conditions for movement
        if (!OutOfBounds(cube.transform.localPosition + behavior.dirVector))
        {
            CubeEvents.InvokeCubeBlocked(cube.transform.position + (behavior.dirVector * 0.6f), behavior.dirVector);
            return;
        }

        Vector3Int newIndexVector = ConvertToGridIndex(cube.transform.localPosition + behavior.dirVector);

        if (!IsValidPosition(newIndexVector) || cubeSlots[newIndexVector.x, newIndexVector.y, newIndexVector.z]) return;

        Vector3Int oldIndexVector = ConvertToGridIndex(cube.transform.localPosition);

        //Matrix empty old slot
        if (!IsValidPosition(oldIndexVector)) return;

        cubeSlots[oldIndexVector.x, oldIndexVector.y, oldIndexVector.z] = null;

        //Movement Execution
        moveService.SetMovementBehavior(behavior);
        moveService.Move(cube.gameObject);

        //Matrix fill new slot
        cubeSlots[newIndexVector.x, newIndexVector.y, newIndexVector.z] = cube.gameObject;
    }

    private void HandleCubeSelection(Transform cube)
    {
        //Outline
        if (this.cube)
        {
            if (this.cube.TryGetComponent(out Cube cubeScript))
            {
                cubeScript.CubeRenderer.materials[1].SetFloat("_Scale", 0);
            }
        }

        if (cube.TryGetComponent(out Cube cubeScript1))
        {
            cubeScript1.CubeRenderer.materials[1].SetFloat("_Scale", outlineThickness);
        }

        //Reassign cube value
        this.cube = cube.GetComponent<Cube>();

        //Check for arrows reference
        if (!arrows) return;

        //Set arrows at selected cube position
        arrows.transform.parent = cube;
        arrows.transform.localPosition = Vector3.zero;
        arrows.SetActive(true);
    }

    #endregion

    #region Behaviors

    private void Solve()
    {
        StartCoroutine(SolveCoroutine());
    }

    #endregion

    #region Utility

    private void FillMatrix()
    {
        if (!map) return;

        foreach (Transform child in map)
        {
            Vector3Int indexVector = ConvertToGridIndex(child.localPosition);

            if (IsValidPosition(indexVector)) cubeSlots[indexVector.x, indexVector.y, indexVector.z] = child.gameObject;
        }
    }

    private void RandomizeCubes()
    {
        if (!map) return;

        foreach (Transform child in map)
        {
            if (child.TryGetComponent(out Cube cube))
            {
                Vector3Int randomEmptyPos = GetRandomEmptyPosition();
                Vector3Int indexVector = ConvertToGridIndex(child.localPosition);

                cubeSlots[indexVector.x, indexVector.y, indexVector.z] = null;

                if (randomEmptyPos != Vector3Int.zero)
                {
                    child.localPosition = ConvertToLocalPosition(randomEmptyPos);
                    cubeSlots[randomEmptyPos.x, randomEmptyPos.y, randomEmptyPos.z] = cube.gameObject;
                }
            }
        }
    }

    private Vector3Int GetRandomEmptyPosition()
    {
        List<Vector3Int> emptyPositions = new List<Vector3Int>();

        // Collect all empty positions
        for (int x = 0; x < cubeSlots.GetLength(0); x++)
        {
            for (int y = 0; y < cubeSlots.GetLength(1); y++)
            {
                for (int z = 0; z < cubeSlots.GetLength(2); z++)
                {
                    if (cubeSlots[x, y, z] == null)
                    {
                        emptyPositions.Add(new Vector3Int(x, y, z));
                    }
                }
            }
        }

        // Return a random empty position
        if (emptyPositions.Count > 0)
        {
            return emptyPositions[Random.Range(0, emptyPositions.Count)];
        }

        // Return Vector3Int.zero if no empty positions are available
        return Vector3Int.zero;
    }

    private bool OutOfBounds(Vector3 position)
    {
        if (position.x < gridLimits.y || position.x > gridLimits.x)
            return false;

        if (position.y < gridLimits.y || position.y > gridLimits.x)
            return false;

        if (position.z < gridLimits.y || position.z > gridLimits.x)
            return false;

        return true;
    }

    private bool IsValidPosition(Vector3Int position)
    {
        return position.x >= 0 && position.x <= cubeSlots.GetLength(0) &&
               position.y >= 0 && position.y <= cubeSlots.GetLength(1) &&
               position.z >= 0 && position.z <= cubeSlots.GetLength(2);
    }

    private Vector3Int ConvertToGridIndex(Vector3 position)
    {
        int x = Mathf.Clamp(Mathf.RoundToInt(position.x + 1), 0, cubeSlots.GetLength(0));
        int y = Mathf.Clamp(Mathf.RoundToInt(position.y + 1), 0, cubeSlots.GetLength(1));
        int z = Mathf.Clamp(Mathf.RoundToInt(position.z + 1), 0, cubeSlots.GetLength(2));

        return new Vector3Int(x, y, z);
    }

    private Vector3 ConvertToLocalPosition(Vector3Int gridIndex)
    {
        float x = gridIndex.x - 1;
        float y = gridIndex.y - 1;
        float z = gridIndex.z - 1;

        return new Vector3(x, y, z);
    }

    #endregion

    #region Coroutines

    private IEnumerator SolveCoroutine()
    {
        foreach(Transform child in map)
        {
            if (child.TryGetComponent(out Cube cube))
            {
                cube.transform.localPosition = cube.InitialPos;
            }

            yield return null;
        }

        UIEvents.InvokeExecuteMove();

        yield return null;
    }

    #endregion
}