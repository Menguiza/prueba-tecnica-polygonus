using UnityEngine;
using UnityEngine.UI;

[RequireComponent (typeof(Button))]
public class Arrow : MonoBehaviour
{
    //Assignable
    [SerializeField] private EDirections direction;

    //IoC
    private IArrowDirectionService arrowDirectionService;

    //Utility
    private Button selfButton;

    //Access
    public EDirections Directions { get { return direction; } }

    private void Start()
    {
        //Get reference
        selfButton = GetComponent<Button>();

        arrowDirectionService = new ArrowDirectionService();

        UIEvents.OnSolving += DisableInteraction;
    }

    private void OnDestroy()
    {
        UIEvents.OnSolving -= DisableInteraction;
    }

    #region Behaviors

    public void SendDirection()
    {
        CubeEvents.InvokeArrowDirection(arrowDirectionService.MovementBehavior(direction));
    }

    #endregion

    #region Utility

    private void DisableInteraction()
    {
        selfButton.enabled = false;
    }    

    #endregion
}
