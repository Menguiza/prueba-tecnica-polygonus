using UnityEngine;

[RequireComponent(typeof(ParticleSystem))]
public class ParticlePoolElement : MonoBehaviour, IPoolElement
{
    //Assignable
    [SerializeField] private float lifeTime = 2f;

    //Utility
    [HideInInspector] public ParticlePool pool;
    private ParticleSystem _particleSystem;

    private void OnEnable()
    {
        Invoke("Recycle", lifeTime);
    }

    private void Awake()
    {
        _particleSystem = GetComponent<ParticleSystem>();
    }

    public void Activate()
    {
        _particleSystem.Play();
    }

    public void Recycle()
    {
        if (!pool) return;

        pool.ReturnParticle(gameObject);
    }
}
