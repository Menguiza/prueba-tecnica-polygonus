using System.Collections.Generic;
using UnityEngine;

public class ParticlePool : MonoBehaviour
{
    [Header("References")]
    [SerializeField] GameObject particlePrefab;

    [Header("Pool Parameters")]
    [SerializeField] public uint poolInitialSize = 10;
    [SerializeField] public uint poolIncrement = 5;

    private IParticleSpawnService particleSpawnService;

    private Queue<GameObject> pool =  new Queue<GameObject>();

    private void Awake()
    {
        particleSpawnService = new ParticleSpawnService();

        FillPool(poolInitialSize);

        CubeEvents.OnCubeBlocked += HandleParticleSpawn;
        GameEvents.OnCastParticle += HandleParticleSpawn;
    }

    private void OnDestroy()
    {
        CubeEvents.OnCubeBlocked -= HandleParticleSpawn;
        GameEvents.OnCastParticle -= HandleParticleSpawn;
    }

    #region Pool Behaviors

    public GameObject GetParticle()
    {
        if (pool.Count == 0)
        {
            FillPool(poolIncrement);
            return GetParticle();
        }

        GameObject poolElement = pool.Dequeue();
        poolElement.SetActive(true);
        poolElement.transform.parent = null;
        poolElement.GetComponent<IPoolElement>().Activate();

        return poolElement;
    }

    public void ReturnParticle(GameObject poolElement)
    {
        pool.Enqueue(poolElement);
        poolElement.SetActive(false);
        poolElement.transform.parent = transform;
        poolElement.transform.localPosition = Vector3.zero;
    }

    public void FillPool(uint size)
    {
        GameObject poolElement;

        for (int i = 0; i < size; i++)
        {
            poolElement = Instantiate(particlePrefab, transform.position, Quaternion.identity, transform);
            poolElement.GetComponent<ParticlePoolElement>().pool = this;
            poolElement.SetActive(false);

            pool.Enqueue(poolElement);
        }
    }

    #endregion

    #region Handlers

    private void HandleParticleSpawn(Vector3 spawnPoint, Vector3 spawnDir)
    {
        particleSpawnService.InstanciateParticle(spawnPoint, spawnDir, GetParticle().transform);
    }

    #endregion
}