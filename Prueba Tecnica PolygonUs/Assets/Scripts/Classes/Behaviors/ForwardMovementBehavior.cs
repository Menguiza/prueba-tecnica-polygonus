using System;
using UnityEngine;

public class ForwardMovementBehavior : IMovementBehavior
{
    public Vector3 dirVector { get => Vector3.forward; }

    public event Action<IMovementBehavior> OnMoveCompleted;

    public void InvokeMoveCompleted()
    {
        OnMoveCompleted?.Invoke(this);
    }

    public void Move(GameObject gameObject)
    {
        gameObject.transform.Translate(dirVector);

        InvokeMoveCompleted();
    }
}