using DG.Tweening;
using System;
using UnityEngine;

public class WalkForwardBehavior : IMovementBehavior
{
    public Vector3 dirVector { get => Vector3.forward; }

    public event Action<IMovementBehavior> OnMoveCompleted;

    public void InvokeMoveCompleted()
    {
        OnMoveCompleted?.Invoke(this);
    }

    public void Move(GameObject gameObject)
    {
        DOTween.To(() => gameObject.transform.position, x => gameObject.transform.position = x, gameObject.transform.position + gameObject.transform.forward*0.7f, 2f).
            OnComplete(InvokeMoveCompleted);
    }
}