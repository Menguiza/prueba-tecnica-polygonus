using DG.Tweening;
using System;
using UnityEngine;

public class RotateRightBehavior : IMovementBehavior
{
    public Vector3 dirVector { get => Vector3.zero; }

    public event Action<IMovementBehavior> OnMoveCompleted;

    public void InvokeMoveCompleted()
    {
        OnMoveCompleted?.Invoke(this);
    }

    public void Move(GameObject gameObject)
    {
        gameObject.transform.DORotate(new Vector3(0, 90, 0), 0.1f, RotateMode.LocalAxisAdd)
            .OnComplete(InvokeMoveCompleted);
    }
}