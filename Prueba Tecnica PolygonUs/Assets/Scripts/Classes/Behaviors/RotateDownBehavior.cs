using UnityEngine;
using DG.Tweening;
using System;

public class RotateDownBehavior : IMovementBehavior
{
    GameObject objToMove;
    public Vector3 dirVector { get => Vector3.forward; }

    public event Action<IMovementBehavior> OnMoveCompleted;

    public void InvokeMoveCompleted()
    {
        OnMoveCompleted?.Invoke(this);
    }

    public void Move(GameObject gameObject)
    {
        objToMove = gameObject;

        DOTween.To(() => gameObject.transform.position, x => gameObject.transform.position = x, gameObject.transform.position + objToMove.transform.forward * 0.5f, 0.25f).
            OnComplete(Rotate);
    }

    private void ForwardMove()
    {
        if (!objToMove) return;

        DOTween.To(() => objToMove.transform.position, x => objToMove.transform.position = x, objToMove.transform.position + objToMove.transform.forward * 0.5f, 0.25f).
            OnComplete(InvokeMoveCompleted);
    }

    private void Rotate()
    {
        if (!objToMove) return;

        DOTween.To(() => objToMove.transform.rotation, x => objToMove.transform.rotation = x, new Vector3(objToMove.transform.rotation.eulerAngles.x + 90, objToMove.transform.rotation.eulerAngles.y, objToMove.transform.rotation.eulerAngles.z), 0.1f).OnComplete(ForwardMove);
    }
}