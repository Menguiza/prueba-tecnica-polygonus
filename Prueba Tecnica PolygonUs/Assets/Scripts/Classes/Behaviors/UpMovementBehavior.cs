using System;
using UnityEngine;

public class UpMovementBehavior : IMovementBehavior
{
    public Vector3 dirVector { get => Vector3.up; }

    public event Action<IMovementBehavior> OnMoveCompleted;

    public void InvokeMoveCompleted()
    {
        OnMoveCompleted?.Invoke(this);
    }

    public void Move(GameObject gameObject)
    {
        gameObject.transform.Translate(dirVector);

        InvokeMoveCompleted();
    }
}