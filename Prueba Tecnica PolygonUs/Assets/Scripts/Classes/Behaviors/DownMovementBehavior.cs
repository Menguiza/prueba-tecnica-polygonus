using System;
using UnityEngine;

public class DownMovementBehavior : IMovementBehavior
{
    public Vector3 dirVector => Vector3.down;

    public event Action<IMovementBehavior> OnMoveCompleted;

    public void InvokeMoveCompleted()
    {
        OnMoveCompleted?.Invoke(this);
    }

    public void Move(GameObject gameObject)
    {
        gameObject.transform.Translate(dirVector);

        InvokeMoveCompleted();
    }
}