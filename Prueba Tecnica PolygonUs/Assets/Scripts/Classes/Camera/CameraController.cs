using Unity.VisualScripting;
using UnityEngine;

[RequireComponent(typeof(Camera))]
public class CameraController : MonoBehaviour
{
    //Assignable
    [Header("References")]
    [SerializeField] private Transform target;
    [SerializeField] private GameObject lastCamera;

    [Header("Parameters")]
    [SerializeField] [Range(1f, 10f)] private float rotateSpeed = 5f;
    [SerializeField] [Range(1f, 100f)] private float scrollSpeed = 17f;
    [SerializeField] [Range(36f, 100f)] private float maxFov = 100f;
    [SerializeField] [Range(5f, 35f)] private float minFov = 35f;

    //Utility
    private Camera cam;

    void Start()
    {
        //SetUp
        cam = GetComponent<Camera>();

        GameEvents.OnLoverFound += TransitionToLastCam;
    }

    void Update()
    {
        //Rotation
        CameraRotation();

        //Fov Adjust
        cam.fieldOfView = CameraZoom();
    }

    private void OnDestroy()
    {
        GameEvents.OnLoverFound += TransitionToLastCam;
    }

    #region Behaviors

    private void CameraRotation()
    {
        if (Input.GetMouseButton(1))
        {
            transform.RotateAround(target.position, Vector3.up, Input.GetAxis("Mouse X") * rotateSpeed);
            transform.RotateAround(target.position, transform.right, Input.GetAxis("Mouse Y") * -rotateSpeed);
        }
    }

    private float CameraZoom()
    {
        float fov = cam.fieldOfView;

        fov += Input.GetAxis("Mouse ScrollWheel") * -scrollSpeed;
        fov = Mathf.Clamp(fov, minFov, maxFov);

        return fov;
    }

    private void TransitionToLastCam()
    {
        if(lastCamera)
        {
            lastCamera.SetActive(true);
            gameObject.SetActive(false);
        }
    }

    #endregion
}
