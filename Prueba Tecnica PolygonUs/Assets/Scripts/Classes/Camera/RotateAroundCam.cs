using System.Collections;
using UnityEngine;

public class RotateAroundCam : MonoBehaviour
{
    [Header("Rotation")]
    [SerializeField] private Transform target;

    [Header("Game Flow")]
    [SerializeField] private UIScreen initialScreen;
    [SerializeField] private GameObject nextCam;

    private float timer, time = 5f;
    private bool rotationFinished = true;

    private void Awake()
    {
        GameEvents.OnRotateCamera += StartProtocol;
    }

    private void OnDestroy()
    {
        GameEvents.OnRotateCamera -= StartProtocol;
    }

    // Update is called once per frame
    void Update()
    {
        RotateCamera();
    }

    #region Utility

    private void StartProtocol()
    {
        rotationFinished = false;
        if (initialScreen) UIEvents.InvokeChangeScreen(initialScreen.Id);
    }

    private void RotateCamera()
    {
        if (rotationFinished) return;

        timer += Time.deltaTime;

        if (timer < time)
        {
            if (target) transform.RotateAround(target.position, Vector3.up, (360 / time) * Time.deltaTime);
        }
        else
        {
            rotationFinished = true;

            StartCoroutine(RandomizeProtocol());
        }
    }

    #endregion

    #region Corroutines

    private IEnumerator RandomizeProtocol()
    {
        GameEvents.InvokeCastParticle(transform.position + transform.forward, -transform.forward);
        GameEvents.InvokeRandomizeCubes();

        yield return new WaitForSeconds(2);

        UIEvents.InvokeChangeScreen("Main");
        nextCam.SetActive(true);
        gameObject.SetActive(false);

        yield return null;
    }

    #endregion
}
