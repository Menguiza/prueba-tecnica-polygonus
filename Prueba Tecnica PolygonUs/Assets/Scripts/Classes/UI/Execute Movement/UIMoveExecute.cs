using UnityEngine;
using UnityEngine.UI;

[RequireComponent (typeof(Button))]
public class UIMoveExecute : MonoBehaviour
{
    //Assignable
    [SerializeField] private GameObject playIcon, restartIcon;

    //Utility
    private bool flag;
    private Button selfButon;

    private void Start()
    {
        //Get reference
        selfButon = GetComponent<Button>();

        UIEvents.OnSolving += DisableInteraction;
    }

    private void OnDestroy()
    {
        UIEvents.OnSolving -= DisableInteraction;
    }

    #region Behaviors

    public void ToggleExecuteMovement()
    {
        if (flag)
        {
            flag = false;
            UIEvents.InvokeAbortMove();
        }
        else
        {
            flag = true;
            UIEvents.InvokeExecuteMove();
        }

        ToggleIcons();
    }

    #endregion

    #region Utility

    private void ToggleIcons()
    {
        if (playIcon) playIcon.SetActive(!playIcon.activeSelf);
        if (restartIcon) restartIcon.SetActive(!restartIcon.activeSelf);
    }

    private void DisableInteraction()
    {
        selfButon.interactable = false;
    }

    #endregion
}
