using UnityEngine;
using UnityEngine.UI;

[RequireComponent (typeof(Button))]
public class Solve: MonoBehaviour
{
    //Utility
    private Button solveButton;

    private void Start()
    {
        //Get reference
        solveButton = GetComponent<Button>();

        UIEvents.OnSolving += DisableInteraction;
        UIEvents.OnExecuteMove += DisableInteraction;
        UIEvents.OnAbortMove += EnableInteraction;
    }

    private void OnDestroy()
    {
        UIEvents.OnSolving -= DisableInteraction;
        UIEvents.OnExecuteMove -= DisableInteraction;
        UIEvents.OnAbortMove -= EnableInteraction;
    }

    #region Behaviors

    public void ExecuteSolving()
    {
        UIEvents.InvokeSolving();
    }

    #endregion

    #region Utility

    private void EnableInteraction()
    {
        solveButton.interactable = true;
    }

    private void DisableInteraction()
    { 
        solveButton.interactable = false;
    }

    #endregion
}
