using UnityEngine;

[RequireComponent (typeof(CanvasGroup))]
public class UIScreen : MonoBehaviour
{
    //Assignable
    [Header("Identification")]
    [SerializeField] private string id;

    //Utility
    private CanvasGroup canvasGroup;

    //Accessors
    public string Id { get { return id; } }
    public CanvasGroup CanvasGroup {  get { return canvasGroup; } }

    private void Awake()
    {
        canvasGroup = GetComponent<CanvasGroup>();
    }
}