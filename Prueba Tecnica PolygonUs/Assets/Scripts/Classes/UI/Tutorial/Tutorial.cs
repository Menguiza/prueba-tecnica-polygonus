using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class Tutorial : MonoBehaviour
{
    //Assigable
    [Header("Step Selection")]
    [SerializeField] private Color offColor = Color.grey;
    [SerializeField] private Color onColor = Color.white;
    [SerializeField] private List<Image> selectionPoints = new List<Image>();
    [SerializeField] private List<CanvasGroup> steps = new List<CanvasGroup>();

    [Header("Game Flow")]
    [SerializeField] private TMP_Text skipText;

    //Utility
    private sbyte count = 0;
    private CanvasGroup currentStep = null;
    private Image lastSelectedPoint = null;
    private bool firstTime = true;

    private void Start()
    {
        ResetStep();
    }

    #region Behavior

    public void ChangeStep(int dir)
    {
        if (!currentStep) return;

        switch (dir)
        {
            case 1:
                count++;
                break;
            case -1:
                count--;
                break;
            default:
                return;
        }

        if (count >= selectionPoints.Count)
        {
            count = 0;
        }
        else if (count < 0)
        {
            count = (sbyte)(selectionPoints.Count - 1);
        }

        if(count == selectionPoints.Count - 1)
        {
            if (skipText) skipText.text = "Play";
        }

        ToggleSelection(currentStep, lastSelectedPoint, false);

        CanvasGroup newStep = steps.ElementAt(count);
        Image newPoint = selectionPoints.ElementAt(count);

        ToggleSelection(newStep, newPoint, true);

        currentStep = newStep;
        lastSelectedPoint = newPoint;
    }

    public void Skip()
    {
        if(firstTime)
        {
            firstTime = false;

            skipText.text = "Play";
            GameEvents.InvokeRotateCamera();
        }
        else
        {
            UIEvents.InvokeChangeScreen("Main");
        }
    }

    #endregion

    #region Utility

    private void ResetStep()
    {
        if (steps.Count > 0)
        {
            count = 0;

            currentStep = steps.ElementAt(0);
            lastSelectedPoint = selectionPoints.ElementAt(0);

            ToggleSelection(currentStep, lastSelectedPoint, true);
        }
    }

    private void ToggleSelection(CanvasGroup step, Image point, bool state)
    {
        step.alpha = state ? 1 : 0;
        step.interactable = state;
        step.blocksRaycasts = state;

        point.color = state ? onColor : offColor;
    }

    #endregion
}
