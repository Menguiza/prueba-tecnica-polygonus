using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class UIManager : MonoBehaviour
{
    //Assignable
    [Header("Button Pressing")]
    [SerializeField] private AudioClip buttonSound;

    [Header("Screens")]
    [SerializeField] private UIScreen loadingScreen;
    [SerializeField] private List<UIScreen> screens = new List<UIScreen>();

    [Header("Scene Loading")]
    [SerializeField] private float loadAsyncDelay;
    [SerializeField] private float loadDelay;
    [SerializeField] private Animator fade;
    [SerializeField] private bool initialFade;

    private void Start()
    {
        if (fade && initialFade) fade.SetTrigger("FadeIn");

        UIEvents.OnChangeScreen += ChangeScreen;
        GameEvents.OnLoverFound += LoversFound;
    }

    private void OnDestroy()
    {
        UIEvents.OnChangeScreen -= ChangeScreen;
        GameEvents.OnLoverFound -= LoversFound;
    }

    #region Behaviors

    public void CastButtonSound()
    {
        UIEvents.InvokeUISFX(buttonSound);
    }

    public void ChangeScreen(string id)
    {
        //Make sure all screens are off
        ToggleScreens(false);

        //Turn on specified screen
        ToggleScreens(true, id);
    }

    public void ChangeScene(int index)
    {
        if(loadingScreen) ChangeScreen(loadingScreen.Id);

        StartCoroutine(LoadLevelAsync(index));
    }

    public void Quit()
    {
        Application.Quit();
    }

    #endregion

    #region Utility

    private void LoversFound()
    {
        ToggleScreens(false);

        StartCoroutine(LoadLevel(SceneManager.sceneCountInBuildSettings-1));
    }

    private void ToggleScreens(bool state)
    {
        foreach (UIScreen s in screens)
        {
            s.CanvasGroup.alpha = state ? 1f : 0f;
            s.CanvasGroup.interactable = state;
            s.CanvasGroup.blocksRaycasts = state;
        }
    }

    private void ToggleScreens(bool state, UIScreen screen)
    {
        screen.CanvasGroup.alpha = state ? 1f : 0f;
        screen.CanvasGroup.interactable = state;
        screen.CanvasGroup.blocksRaycasts = state;
    }

    private void ToggleScreens(bool state, string screenId)
    {
        foreach (UIScreen s in screens)
        {
            if(s.Id.ToUpper() == screenId.ToUpper())
            {
                ToggleScreens(state, s);
                break;
            }
        }
    }

    private void ToggleScreens(bool state, List<UIScreen> screenList)
    {
        foreach (UIScreen s in screenList)
        {
            s.CanvasGroup.alpha = state ? 1f : 0f;
            s.CanvasGroup.interactable = state;
            s.CanvasGroup.blocksRaycasts = state;
        }
    }

    #endregion

    #region Coroutines

    private IEnumerator LoadLevelAsync(int index)
    {
        AsyncOperation loadingLevel = SceneManager.LoadSceneAsync(index);
        loadingLevel.allowSceneActivation = false;

        while (loadingLevel.progress < 0.9f) 
        {
            UIEvents.InvokeSceneLoading(Mathf.Clamp01(loadingLevel.progress / 0.9f));

            yield return new WaitForEndOfFrame();
        }

        if (fade) fade.SetTrigger("FadeOut");

        UIEvents.InvokeSceneLoading(Mathf.Clamp01(loadingLevel.progress / 0.9f));

        yield return new WaitForSeconds(loadAsyncDelay);

        loadingLevel.allowSceneActivation = true;

        yield return null;
    }

    private IEnumerator LoadLevel(int index)
    {
        yield return new WaitForSeconds(loadDelay-1);

        if (fade) fade.SetTrigger("FadeOut");

        yield return new WaitForSeconds(1);

        SceneManager.LoadScene(index);
    }

    #endregion
}
