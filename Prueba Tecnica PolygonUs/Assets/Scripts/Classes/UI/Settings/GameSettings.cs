using UnityEngine;
using UnityEngine.Audio;
using UnityEngine.UI;

[RequireComponent(typeof(UIScreen))]
public class GameSettings : MonoBehaviour
{
    //Assignables
    [Header("Sound")]
    [SerializeField] private AudioMixer audioMixer;
    [SerializeField] private Slider masterSlider, sfxSlider, musicSlider;

    [Header("Screen")]
    [SerializeField] private Toggle fullScreen;

    [Header("UI Escape Feature")]
    [SerializeField] private UIScreen mainScreen;

    //Utility
    private UIScreen mScreen;

    private void Start()
    {
        //Get reference
        mScreen = GetComponent<UIScreen>();

        if (masterSlider)
        {
            if(PlayerPrefs.HasKey("MasterVolume"))
            {
                masterSlider.value = PlayerPrefs.GetFloat("MasterVolume");
            }

            masterSlider.onValueChanged.AddListener(MasterVolume);

            MasterVolume(masterSlider.value);
        }
        if (sfxSlider)
        {
            if (PlayerPrefs.HasKey("SFXVolume"))
            {
                sfxSlider.value = PlayerPrefs.GetFloat("SFXVolume");
            }

            sfxSlider.onValueChanged.AddListener(SFXVolume);

            SFXVolume(sfxSlider.value);
        }
        if (musicSlider)
        {
            if (PlayerPrefs.HasKey("MusicVolume"))
            {
                musicSlider.value = PlayerPrefs.GetFloat("MusicVolume");
            }

            musicSlider.onValueChanged.AddListener(MusicVolume);

            MusicVolume(musicSlider.value);
        }

        if (fullScreen)
        {
            fullScreen.isOn = Screen.fullScreen;

            fullScreen.onValueChanged.AddListener(FullScreenToggle);
        }
    }

    private void Update()
    {
        if(Input.GetKeyDown(KeyCode.Escape))
        {
            if(mScreen.CanvasGroup.alpha == 0)
            {
                UIEvents.InvokeChangeScreen(mScreen.Id);
            }
            else
            {
                if(mainScreen) UIEvents.InvokeChangeScreen(mainScreen.Id);
            }
        }
    }

    #region Behaviors

    private void MasterVolume(float volume)
    {
        UpdateVolume("MasterVolume", volume);
    }

    private void SFXVolume(float volume)
    {
        UpdateVolume("SFXVolume", volume);
    }

    private void MusicVolume(float volume)
    {
        UpdateVolume("MusicVolume", volume);
    }

    private void FullScreenToggle(bool toggle)
    {
        Screen.SetResolution(1920, toggle?1080:1040, toggle);
    }

    #endregion

    #region Utility

    private void UpdateVolume(string paramName, float volume)
    {
        PlayerPrefs.SetFloat(paramName, volume);

        audioMixer.SetFloat(paramName, Mathf.Log10(volume) * 20);
    }

    #endregion
}
