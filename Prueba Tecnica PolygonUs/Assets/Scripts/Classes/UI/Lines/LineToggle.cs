using UnityEngine;
using UnityEngine.UI;

[RequireComponent (typeof(Image))]
public class LineToggle : MonoBehaviour
{
    //Asignable
    [Header("Toggle Parameters")]
    [SerializeField] private Color disabledColor = Color.gray;
    [SerializeField] private Color enabledColor = Color.white;

    //Utility
    private Image img;
    private bool flag = true;

    private void Awake()
    {
        //Get reference
        img = GetComponent<Image> ();
    }

    #region Behaviors

    public void TriggerToggle()
    {
        CubeEvents.InvokeToggleLines();

        if (flag)
        {
            img.color = disabledColor;
            flag = false;
        }
        else
        {
            img.color = enabledColor;
            flag = true;
        }
    }

    #endregion
}
