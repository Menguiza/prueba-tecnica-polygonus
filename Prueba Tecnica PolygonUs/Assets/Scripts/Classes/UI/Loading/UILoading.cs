using UnityEngine;
using UnityEngine.UI;

public class UILoading : MonoBehaviour
{
    //Assignable
    [Header("Loading")]
    [SerializeField] private Slider loadingSlider;

    private void Start()
    {
        UIEvents.OnSceneLoading += UpdatePercentage;
    }

    private void OnDestroy()
    {
        UIEvents.OnSceneLoading -= UpdatePercentage;
    }

    #region Behaviors

    private void UpdatePercentage(float percentage)
    {
        if(loadingSlider) loadingSlider.value = percentage;
    }

    #endregion
}
