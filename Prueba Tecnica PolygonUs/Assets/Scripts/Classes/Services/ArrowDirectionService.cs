public class ArrowDirectionService : IArrowDirectionService
{
    public IMovementBehavior MovementBehavior(EDirections direction)
    {
        switch (direction)
        {
            case EDirections.Forward:
                return new ForwardMovementBehavior();
            case EDirections.Backward:
                return new BackwardMovementBehavior();
            case EDirections.Up:
                return new UpMovementBehavior();
            case EDirections.Down:
                return new DownMovementBehavior();
            case EDirections.Right:
                return new RightMovementBehavior();
            case EDirections.Left:
                return new LeftMovementBehavior();
            default:
                return null;
        }
    }
}