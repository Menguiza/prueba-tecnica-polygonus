using System;
using UnityEngine;

public class MoveCubeService : IMoveService
{
    private IMovementBehavior movementBehavior;

    public void SetMovementBehavior(IMovementBehavior newBehavior)
    {
        if(movementBehavior is IDisposable disposable)
        {
            disposable.Dispose();
        }

        movementBehavior = newBehavior;
    }

    public void Move(GameObject cube)
    {
        if (movementBehavior == null) return;

        movementBehavior.Move(cube);
        CubeEvents.InvokeCubeMove(cube);
    }
}