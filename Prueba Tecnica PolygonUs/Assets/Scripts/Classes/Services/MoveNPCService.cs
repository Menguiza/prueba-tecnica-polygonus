using System;
using UnityEngine;

public class MoveNPCService : IMoveService
{
    private IMovementBehavior movementBehavior;

    public void SetMovementBehavior(IMovementBehavior newBehavior)
    {
        if (movementBehavior is IDisposable disposable)
        {
            disposable.Dispose();
        }

        movementBehavior = newBehavior;
    }

    public void Move(GameObject npc)
    {
        if (movementBehavior == null) return;

        movementBehavior.Move(npc);
    }
}