using UnityEngine;

public class ParticleSpawnService : IParticleSpawnService
{
    public void InstanciateParticle(Vector3 spawnPoint, Vector3 direction, Transform particle)
    {
        particle.position = spawnPoint;
        particle.forward = direction;
    }
}