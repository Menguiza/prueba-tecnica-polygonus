using System;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class NPC : MonoBehaviour, INpc
{
    //Assiganble
    [Header("Cube Detection")]
    [SerializeField] private Transform checkRayOrigin;
    [SerializeField] [Range(0f, 100f)] private float rayLength = 5f;
    [SerializeField] private LayerMask cubeLayer;

    [Header("Lover Detection")]
    [SerializeField] private bool loverChecker;
    [SerializeField] private Transform loverRayOrigin;
    [SerializeField][Range(0f, 100f)] private float loverRayLength = 5f;
    [SerializeField] private LayerMask loverLayer;
    [SerializeField] private GameObject gift;

    [Header("Animtaion Related")]
    [SerializeField] Animator animator;

    //Utility
    private float rayAngle = 0.6f;
    private StateMachine stateMachine = new StateMachine();
    private Queue<EDirections> movePath = new Queue<EDirections>();
    private Vector3 initialPos = Vector3.zero;
    private Quaternion initialRot;

    //Events
    public event Action<List<EDirections>> OnPathFound;
    public event Action OnEndMove;
    public event Action OnPathNotFound;
    public event Action OnLoverReached;

    //Accessors
    public Animator Animator { get { return animator; } }

    private void Start()
    {
        initialPos = transform.position;
        initialRot = transform.rotation;

        stateMachine.ChangeState(new IdleState(this));

        UIEvents.OnExecuteMove += StartPathSearch;
        UIEvents.OnAbortMove += RestartPathSearch;
        GameEvents.OnLoverFound += LoverReachProtocol;
        OnPathFound += AddDirections;
        OnPathNotFound += CheckForLover;
        OnEndMove += ContinuePath;
        OnLoverReached += LoverReached;
    }

    void Update()
    {
        stateMachine.Update();
    }

    private void OnDestroy()
    {
        UIEvents.OnExecuteMove -= StartPathSearch;
        UIEvents.OnAbortMove -= RestartPathSearch;
        GameEvents.OnLoverFound -= LoverReachProtocol;
        OnPathFound -= AddDirections;
        OnEndMove -= ContinuePath;
        OnPathNotFound -= CheckForLover;
        OnLoverReached -= LoverReached;
    }

    #region Implementation

    public void InvokePathFound(List<EDirections> directions)
    {
        OnPathFound?.Invoke(directions);
    }

    public void InvokePathNotFound()
    {
        OnPathNotFound?.Invoke();
    }

    public void InvokeEndMove()
    {
        OnEndMove?.Invoke();
    }

    public void InvokeLoverReached()
    {
        OnLoverReached?.Invoke();
    }

    #endregion

    #region Behaviors

    private void StartPathSearch()
    {
        stateMachine.ChangeState(new PathingState(this, checkRayOrigin, rayLength, rayAngle, cubeLayer));
    }

    private void RestartPathSearch()
    {
        DOTween.KillAll();

        StopPathing();

        transform.position = initialPos;
        transform.rotation = initialRot;
    }

    #endregion

    #region Utility

    private void AddDirections(List<EDirections> directions)
    {
        foreach(EDirections direction in directions)
        {
            movePath.Enqueue(direction);
        }

        stateMachine.ChangeState(new MovingState(this, movePath));
    }

    private void ContinuePath()
    {
        stateMachine.ChangeState(new PathingState(this, checkRayOrigin, rayLength, rayAngle, cubeLayer));
    }

    private void CheckForLover()
    {
        if (loverChecker && loverRayOrigin) stateMachine.ChangeState(new CheckLoverState(this, loverRayOrigin, loverRayLength, loverLayer));
        else StopPathing();
    }

    private void StopPathing()
    {
        movePath.Clear();

        stateMachine.ChangeState(new IdleState(this));
    }

    private void LoverReachProtocol()
    {
        stateMachine.ChangeState(new LovingState(this));
    }

    private void LoverReached()
    {
        if (Animator) animator.SetTrigger("Gift");
        if(gift) gift.SetActive(true);
    }

    #endregion

#if UNITY_EDITOR

    private void OnDrawGizmos()
    {
        Gizmos.color = Color.yellow;
        Gizmos.DrawRay(checkRayOrigin.position, (checkRayOrigin.forward - checkRayOrigin.up * rayAngle) * rayLength);

        if(loverChecker)
        {
            Gizmos.color = Color.blue;
            Gizmos.DrawRay(loverRayOrigin.position, loverRayOrigin.forward * loverRayLength);
        }
    }

#endif
}
