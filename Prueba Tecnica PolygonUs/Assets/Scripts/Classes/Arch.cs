using UnityEngine;

public class Arch : MonoBehaviour
{
    [Header("Lover Meeting Param")]
    [SerializeField] private ParticleSystem _particleSystem;

    private void Start()
    {
        GameEvents.OnLoverFound += ActivateParticles;
    }

    private void OnDestroy()
    {
        GameEvents.OnLoverFound -= ActivateParticles;
    }

    #region Behaviors

    private void ActivateParticles()
    {
        if(_particleSystem) _particleSystem.Play();
    }

    #endregion
}
