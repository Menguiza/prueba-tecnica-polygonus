using System;
using UnityEngine;

public interface IMovementBehavior
{
    event Action<IMovementBehavior> OnMoveCompleted;
    Vector3 dirVector { get; }
    void Move(GameObject gameObject);
    void InvokeMoveCompleted();
}
