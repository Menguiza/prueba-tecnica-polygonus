using UnityEngine;

public interface IMoveService
{
    void SetMovementBehavior(IMovementBehavior movementBehavior);
    void Move(GameObject objectToMove);
}