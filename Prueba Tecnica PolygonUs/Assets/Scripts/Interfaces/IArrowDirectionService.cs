using UnityEngine;

public interface IArrowDirectionService
{
    IMovementBehavior MovementBehavior(EDirections direction);
}