using System.Collections.Generic;

public interface IInstructor
{
    List<EDirections> Instructions { get; }
}