using System;
using System.Collections.Generic;

public interface INpc
{
    event Action<List<EDirections>> OnPathFound;
    event Action OnPathNotFound;
    event Action OnEndMove;
    event Action OnLoverReached;

    void InvokePathFound(List<EDirections> directions);
    void InvokePathNotFound();
    void InvokeEndMove();
    void InvokeLoverReached();
}
