using UnityEngine;

public interface IParticleSpawnService
{
    void InstanciateParticle(Vector3 spawnPoint, Vector3 direction, Transform particle);
}