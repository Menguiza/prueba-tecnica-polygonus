using UnityEngine;

public interface IPoolElement
{
    void Activate();
    void Recycle();
}