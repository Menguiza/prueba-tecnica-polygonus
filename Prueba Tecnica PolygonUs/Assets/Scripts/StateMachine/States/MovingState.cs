using System.Collections.Generic;

public class MovingState : IState
{
    private NPC owner;
    private Queue<EDirections> directions;

    private IMoveService moveService = new MoveNPCService();

    public MovingState(NPC owner, Queue<EDirections> directions) { this.owner = owner; this.directions = directions; }

    public void Enter()
    {
        EDirections dir = directions.Dequeue();
        IMovementBehavior behavior = EvaluateBehavior(dir);

        behavior.OnMoveCompleted += PathMove;

        moveService.SetMovementBehavior(behavior);
        moveService.Move(owner.gameObject);

        if (owner.Animator) owner.Animator.SetTrigger("Move");
    }

    public void Execute()
    {
    }

    public void Exit()
    {
        if (owner.Animator) owner.Animator.ResetTrigger("Move");
    }

    #region Utility

    private void PathMove(IMovementBehavior previousBehavior)
    {
        previousBehavior.OnMoveCompleted -= PathMove;

        if (directions.Count > 0)
        {
            EDirections dir = directions.Dequeue();
            IMovementBehavior behavior = EvaluateBehavior(dir);

            behavior.OnMoveCompleted += PathMove;

            moveService.SetMovementBehavior(behavior);
            moveService.Move(owner.gameObject);
        }
        else
        {
            owner.InvokeEndMove();
        }
    }

    private IMovementBehavior EvaluateBehavior(EDirections dir)
    {
        switch (dir)
        {
            case EDirections.Forward:
                return new ForwardLerpMovementBehavior();
            case EDirections.Backward:
                return null;
            case EDirections.Up:
                return null;
            case EDirections.Down:
                return new RotateDownBehavior();
            case EDirections.Right:
                return new RotateRightBehavior();
            case EDirections.Left:
                return new RotateLeftBehavior();
            default:
                return null;
        }
    }

    #endregion
}