using UnityEngine;

public class IdleState : IState
{
    private NPC owner;

    public IdleState(NPC owner) { this.owner = owner; }

    public void Enter()
    {
        if(owner.Animator) owner.Animator.SetTrigger("Idle");
    }

    public void Execute()
    {
    }

    public void Exit()
    {
        if (owner.Animator) owner.Animator.ResetTrigger("Idle");
    }
}