using UnityEngine;

public class PathingState : IState
{
    private NPC npc;
    private Transform rayOrigin;
    private float rayLength, rayAngle;
    private LayerMask cubeLayer;

    public PathingState(NPC npc, Transform rayOrigin, float rayLength, float rayAngle, LayerMask cubeLayer) 
    { 
        this.npc = npc;
        this.rayOrigin = rayOrigin;
        this.rayLength = rayLength;
        this.rayAngle = rayAngle;
        this.cubeLayer = cubeLayer;
    }

    public void Enter()
    {
    }

    public void Execute()
    {
        CheckForCubes();
    }

    public void Exit()
    {
    }

    #region Utility

    private void CheckForCubes()
    {
        Vector3 rayDirection = rayOrigin.forward - rayOrigin.transform.up * rayAngle;

        // Perform the raycast
        Ray ray = new Ray(rayOrigin.position, rayDirection);
        RaycastHit hit;

        if (Physics.Raycast(ray, out hit, rayLength, cubeLayer))
        {
            if (hit.transform.parent.TryGetComponent(out IInstructor cube))
            {
                npc.InvokePathFound(cube.Instructions);
            }
        }
        else
        {
            npc.InvokePathNotFound();
        }
    }

    #endregion
}