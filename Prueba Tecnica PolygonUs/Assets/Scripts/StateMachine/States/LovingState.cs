public class LovingState : IState
{
    private NPC owner;

    private IMoveService moveService = new MoveNPCService();

    public LovingState(NPC owner) { this.owner = owner; }

    public void Enter()
    {
        IMovementBehavior behavior = new WalkForwardBehavior();

        behavior.OnMoveCompleted += Reached;

        moveService.SetMovementBehavior(behavior);
        moveService.Move(owner.gameObject);

        if (owner.Animator) owner.Animator.SetTrigger("Walk");
    }

    public void Execute()
    {
    }

    public void Exit()
    {
        if (owner.Animator) owner.Animator.ResetTrigger("Walk");
    }

    #region Utility

    private void Reached(IMovementBehavior behavior)
    {
        behavior.OnMoveCompleted -= Reached;

        owner.InvokeLoverReached();
    }

    #endregion
}