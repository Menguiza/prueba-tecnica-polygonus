using UnityEngine;

public class CheckLoverState : IState
{
    private NPC npc;
    private Transform rayOrigin;
    private float rayLength;
    private LayerMask loverLayer;

    public CheckLoverState(NPC npc, Transform rayOrigin, float rayLength, LayerMask loverLayer)
    {
        this.npc = npc;
        this.rayOrigin = rayOrigin;
        this.rayLength = rayLength;
        this.loverLayer = loverLayer;
    }

    public void Enter()
    {
        npc.Animator.SetTrigger("Idle");
    }

    public void Execute()
    {
        CheckForLover();
    }

    public void Exit()
    {
        npc.Animator.ResetTrigger("Idle");
    }

    #region Utility

    private void CheckForLover()
    {
        // Perform the raycast
        Ray ray = new Ray(rayOrigin.position, rayOrigin.forward);
        RaycastHit hit;

        if (Physics.Raycast(ray, out hit, rayLength, loverLayer))
        {
            if (hit.transform.root.TryGetComponent(out NPC npc))
            {
                GameEvents.InvokeLoverFound();
            }
        }
    }

    #endregion
}