using System;
using UnityEngine;

public static class CubeEvents
{
    public static event Action<IMovementBehavior> OnArrowPressed;

    public static event Action<GameObject> OnCubeMove;

    public static event Action<Transform> OnCubeSelected;

    public static event Action<Vector3, Vector3> OnCubeBlocked;

    public static event Action OnToggleLines;

    public static void InvokeCubeBlocked(Vector3 blockPoint, Vector3 blockDir)
    {
        OnCubeBlocked?.Invoke(blockPoint, blockDir);
    }

    public static void InvokeCubeMove(GameObject cube)
    {
        OnCubeMove?.Invoke(cube);
    }

    public static void InvokeArrowDirection(IMovementBehavior behavior) 
    {
        OnArrowPressed?.Invoke(behavior);
    }

    public static void InvokeCubeSelect(Transform cube)
    {
        OnCubeSelected?.Invoke(cube);
    }

    public static void InvokeToggleLines()
    {
        OnToggleLines?.Invoke();
    }
}
