using System;
using UnityEngine;

public static class UIEvents
{
    public static event Action<AudioClip> OnUISFX;
    public static event Action<string> OnChangeScreen;
    public static event Action OnExecuteMove;
    public static event Action OnAbortMove;
    public static event Action OnSolving;
    public static event Action<float> OnSceneLoading;

    public static void InvokeUISFX(AudioClip clip)
    {
        OnUISFX?.Invoke(clip);
    }

    public static void InvokeChangeScreen(string screenId)
    {
        OnChangeScreen?.Invoke(screenId);
    }

    public static void InvokeExecuteMove()
    {
        OnExecuteMove?.Invoke();
    }

    public static void InvokeAbortMove()
    {
        OnAbortMove?.Invoke();
    }

    public static void InvokeSolving()
    {
        OnSolving?.Invoke();
    }

    public static void InvokeSceneLoading(float completedValue)
    {
        OnSceneLoading?.Invoke(completedValue);
    }
}