using System;
using UnityEngine;

public static class GameEvents
{
    public static event Action OnRotateCamera;
    public static event Action<Vector3, Vector3> OnCastParticle;
    public static event Action OnRandomizeCubes;
    public static event Action OnLoverFound;

    public static void InvokeCastParticle(Vector3 position, Vector3 direction)
    {
        OnCastParticle?.Invoke(position, direction);
    }

    public static void InvokeRandomizeCubes()
    {
        OnRandomizeCubes?.Invoke();
    }

    public static void InvokeRotateCamera()
    {
        OnRotateCamera?.Invoke();
    }

    public static void InvokeLoverFound()
    {
        OnLoverFound?.Invoke();
    }
}
